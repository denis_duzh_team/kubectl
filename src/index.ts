import axios from "axios";

const spec: any = {
  containers: [
    {
      image: "dduzh/server",
      label: "backend"
    }
  ],
  services: [
    {
      domain: "my.domain",
      label: "backend",
      port: 3000
    }
  ]
};

axios
  .post("http://localhost:3000/deployment", { spec })
  .then(res => console.log(res.data))
  .catch(console.log);
